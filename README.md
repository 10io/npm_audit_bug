# npm_audit_bug

## Requirements

* A [PAT](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) token

## Steps

* `$ TOKEN='XXX' npm install @10io/ananas`
* `$ TOKEN='XXX' npm audit`
